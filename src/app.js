import React from 'react';
import ReactDOM from 'react-dom';

import TweeterAnalyticsApp from './components/TweeterAnalyticsApp';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

ReactDOM.render(<TweeterAnalyticsApp />, document.getElementById('app'))