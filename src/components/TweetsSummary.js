import React from 'react';

import GenericObjectDisplayer from './GenericObjectDisplayer';

const TweetsSummary = (props) => (
    <div>
        <div className='widget-header'>
            <h3 className='widget-header__title'>{props.title}</h3>
        </div>
        {
            props.tweets_data.length === 0 &&
            <p className='widget__message'>
                Waiting for tweets data...
            </p>
        }
        {
            typeof props.tweets_data === 'number' &&
            <div className='tweetData'>
                <p className='tweetData__text'>{props.tweets_data}</p>
            </div>
        }
        {
            Object.keys(props.tweets_data).map((word) => (
                <GenericObjectDisplayer
                    key={word}
                    word={word}
                    count={props.tweets_data[word]}
                />
            ))
        }
    </div>
);

export default TweetsSummary;