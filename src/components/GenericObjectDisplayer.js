import React from 'react';

const GenericObjectDisplayer = (props) => (
    <div className='tweetData'>
        <p className='tweetData__text'>{props.word}: {props.count}</p>
    </div>
);

export default GenericObjectDisplayer;