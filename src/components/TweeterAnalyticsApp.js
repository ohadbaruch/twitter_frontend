import React from 'react';

import Header from './Header';
import TweetsSummary from './TweetsSummary';

export default class TweeterAnalyticsApp extends React.Component {
    state = {
        isLoaded: false,
        tweets_data: {}
    }
    componentDidMount() {
        fetch("http://localhost:5000")
            .then(res => res.json())
            .then(
                (tweets_data) => {
                    this.setState(() => ({ 
                        isLoaded: true,
                        tweets_data:tweets_data
                    }))
                }
            )
    }
    render() {
        const subtitle = 'Put your life in the hands of a computer';
        if (this.state.isLoaded) {
            return (
                <div>
                    <Header subtitle={subtitle}/>
                    <div className='container body-background'>
                        <div className='widget'>
                            <TweetsSummary title="Top 10 words" tweets_data={this.state.tweets_data.top_10_words} />
                            <TweetsSummary title="Top 10 users" tweets_data={this.state.tweets_data.top_10_users} />
                            <TweetsSummary title="Top 10 hashtags" tweets_data={this.state.tweets_data.top_10_hashtags} />
                            <TweetsSummary title="Average tweets per second" tweets_data={this.state.tweets_data.avg_tweets_per_second} />
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div>
                    "Loading..."
                </div>
            )
        }
    }
}

TweeterAnalyticsApp.defaultProps = {
    tweets_data: {}
}